/*!
\file main.c
\autor Jalbert Sylvain
\version 1
\date 10 mars 2020
\brief fichier principal du programme qui permet à l'utilisateur d'optimiser un problème linéaire
*/

#include "saisie.h"

/*!
\fn int main ( int argc, char∗∗ argv )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 10 mars 2020
\brief la fonction principale qui permet à l'utilisateur d'optimiser un problème linéaire
\param argc nombre d’arguments en entree
\param argv valeur des arguments en entree
\return 0 si tout c'est bien passé
*/
int main (int argc, char** argv){
  //DECLARATION DES VARIABLES

  //INITIALISATION DES VARIABLES

  //Fin du programme, Il se termine normalement, et donc retourne 0
  return(0);
}
