/*!
\file optimisation.c
\autor Jalbert Sylvain
\version 1
\date 10 mars 2020
\brief le fichier qui contient les définitions de toutes les méthodes relatives à l'optimisation d'un problème
*/

#include "optimisation.h"

/*!
\fn
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 10 mars 2020
\brief une fonction qui creer un tableau de tableau de reel
\param int_nbrLigne le nombre de ligne de la matrice
\param int_nbrColonne le nombre de colonne de la matrice
\return le pointeur de la premiere case de la matrice de réel
*/
int pretraitement(float **ttfloat_tab){
  //DECLARATION DES VARIABLES
  int int_estAdmissible; //vraie si le tableau initial est admissible, faux si les contraintes sont vides

  //INITIALISATION DES VARIABLES
  //on considère au debut que le tableau est admissible
  int_estAdmissible = true;

  //retourner si le tableau est admissible ou non
  return(int_estAdmissible);
}
