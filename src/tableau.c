/*!
\file tableau.c
\autor Jalbert Sylvain
\version 1
\date 10 mars 2020
\brief le fichier qui contient les définitions de toutes les méthodes relatives aux manipulations de tableau
*/

#include "tableau.h"

/*!
\fn float **creerMatriceReel(int int_nbrLigne, int int_nbrColonne)
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 10 mars 2020
\brief une fonction qui creer un tableau de tableau de reel
\param int_nbrLigne le nombre de ligne de la matrice
\param int_nbrColonne le nombre de colonne de la matrice
\return le pointeur de la premiere case de la matrice de réel
*/
float **creerMatriceReel(int int_nbrLigne, int int_nbrColonne){
  //DECLARATION DES VARIABLES
  int int_i; //variable qui va parcourir les lignes du tableau
  float **ttfloat_mat; //le poiteur vers la premiere case de la matrice

  //ALLOCATION DE LA MEMOIRE POUR LES LIGNES
  ttfloat_mat = malloc(int_nbrLigne * sizeof(float*));
  //Si l'allocation c'est fini en echec
  if(ttfloat_mat == NULL){
    //Avertir l'utilisateur
    printf("Erreur d'allocation mémoire !");
    //Quitter le programme avec un message d'erreur
    exit(ERREUR_ALLOCATION);
  }

  //ALLOCATION DE LA MEMOIRE POUR LES COLONNES
  for(i = 0 ; i < int_nbrLigne ; i++){
    ttfloat_mat[i] = malloc(int_nbrColonne * sizeof(float));
    //Si l'allocation c'est fini en echec
    if(ttfloat_mat[i] == NULL){
      //Avertir l'utilisateur
      printf("Erreur d'allocation mémoire !");
      //Quitter le programme avec un message d'erreur
      exit(ERREUR_ALLOCATION);
    }
  }

  //retourner l'addresse de la premiere case du tableau, soit : tint_tab
  return(ttfloat_mat);
}

/*!
\fn void detruireMatriceReel(float **ttfloat_mat)
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 10 mars 2020
\brief une procedure qui détruit en mémoire une matrice de reel
\param ttfloat_mat la matrice de réel a détruire
*/
void detruireMatriceReel(float **ttfloat_mat){
  //DECLARATION DES VARIABLES
  int int_i; //variable qui va parcourir les lignes du tableau
  float **ttfloat_mat; //le poiteur vers la premiere case de la matrice

  //LIBERATION DES COLONNES
  for(i = 0 ; i < int_nbrLigne ; i++){
    free(ttfloat_mat[i]);
  }

  //LIBERATION DES LIGNES
  free(ttfloat_mat);
}
