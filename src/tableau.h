/*!
\file tableau.h
\autor Jalbert Sylvain
\version 1
\date 10 mars 2020
\brief le fichier qui contient les déclarations de toutes les méthodes relatives aux manipulations de tableau
*/

#ifndef __TABLEAU_H_
#define __TABLEAU_H_

//onutilise la librairie utile ici pour la gestion de la mémoire et des "exit"
#include <stdlib.h>
//on utilise la librairie utile pour les interactions avec l'utilisateur
#include <stdio.h>


#define ERREUR_ALLOCATION 1

/*!
\fn float **creerMatriceReel(int int_nbrLigne, int int_nbrColonne)
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 10 mars 2020
\brief une fonction qui creer un tableau de tableau de reel
\param int_nbrLigne le nombre de ligne de la matrice
\param int_nbrColonne le nombre de colonne de la matrice
\return le pointeur de la premiere case de la matrice de réel
*/
float **creerMatriceReel(int int_nbrLigne, int int_nbrColonne);

/*!
\fn void detruireMatriceReel(float **ttfloat_mat)
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 10 mars 2020
\brief une procedure qui détruit en mémoire une matrice de reel
\param ttfloat_mat la matrice de réel a détruire
*/
void detruireMatriceReel(float **ttfloat_mat);

#endif
